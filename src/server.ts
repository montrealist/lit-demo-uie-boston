var express = require('express');
var app = express();
var path = require('path');

app.use(express.static('public'));
app.use('/static', express.static(path.join(__dirname, '/components')));
// console.log(path.join(__dirname, '/components'));

app.get('/', function(req: any, res: any) {
    res.sendFile('/index.html');
});

app.listen(8090);